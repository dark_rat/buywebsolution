﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextEditorInintelligence.Classes;
using System.Diagnostics;
using NLog;

namespace TextEditorInintelligence
{
    class Program
    {
        static void Main(string[] args)
        {
            DiagUtils diag = new DiagUtils();
            diag.startSecondSwatch();
            InputFileProvider IFP = new InputFileProvider();
            if (IFP.validateFile())
            {
                DictionaryProvider DP = new DictionaryProvider();
                DP.InputFile = IFP;
                DiagUtils.LogMemUsage("Main");
                diag.startSwatch("Dictionary generate");
                DP.createDictionaryCollection(IFP);
                diag.stopSwatch();                
                DiagUtils.LogMemUsage("Main");
                Searcher search = new Searcher();
                search.DP = DP;
                search.searchingInCollection();
            }
            else
            {
                DiagUtils.ErrorLogList.Add("Input file is not corrected");
            }
            diag.stopSecondSwatch("Working");
            diag.writeLogToFile();
        }
    }
}
