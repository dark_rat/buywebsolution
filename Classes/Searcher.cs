﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace TextEditorInintelligence.Classes
{
    /// <summary>
    /// Обеспечивает поиск слов в словаре
    /// </summary>
    class Searcher
    {
        public DictionaryProvider DP { get; set; }
        private DTO.DictCollection searchDictCollection = new DTO.DictCollection();     

        public Searcher()
        { 

        }

        /// <summary>
        /// Поиск слов по коллекции словарей в памяти.
        /// </summary>        
        public void searchingInCollection()
        {
            DiagUtils diag = new DiagUtils();
            diag.startSecondSwatch();
            OutputFileProvider OFP = new OutputFileProvider();
            OFP.reset();
            ConcurrentBag<DTO.Dict> concurentSearchDictBag = new ConcurrentBag<DTO.Dict>();
            Parallel.ForEach(DP.InputFile.getUserDictWordsList(), sw =>
            {
                DTO.Dict searchDict = new DTO.Dict();                               //Объект для хранения Слова пользователя и подходящих слов
                searchDict.Header = sw.word;
                searchDict.Id = sw.Id;
                searchDict.words.AddRange(searchDictWordsManualy(sw.word, DP.collection.Dictionaries.SingleOrDefault(d => d.Header == sw.word[0].ToString()).words));
                concurentSearchDictBag.Add(searchDict);                  //Складываем в коллекцию.           
            });
            searchDictCollection.Dictionaries.AddRange(concurentSearchDictBag);
            diag.stopSecondSwatch("searching");
            DiagUtils.LogMemUsage("searchingLinqFromCollection");
            diag.startSwatch("searchingLinqFromCollection: Print Dict collection");
            DP.printDictCollection(searchDictCollection, OFP);                     //Поиск по всем словам завершен. Можно порадовать пользователя и явить файл в каталог.
            diag.stopSwatch();
        }

        /// <summary>
        /// Поиск слова в коллекции словарей
        /// </summary>
        /// <param name="_inputWord">Искомое слово</param>
        /// <param name="_dictList">Лист слов из словаря</param>
        /// <returns></returns>      
        internal List<DTO.DictWord> searchDictWordsManualy(string _inputWord, List<DTO.DictWord> _dictList)
        {
            int countWords = 0;
            List<DTO.DictWord> ResultList = new List<DTO.DictWord>();
            foreach (DTO.DictWord word in _dictList)
            {
                if (this.matchWords(_inputWord, word.word))
                {
                    ResultList.Add(word);
                    countWords++;
                }
                if (countWords == 10)
                    break;
            }
            return ResultList;
        }

        /// <summary>
        /// Совпадение слова
        /// </summary>
        /// <param name="_inputWord"></param>
        /// <param name="_dictWord"></param>
        /// <returns></returns>
        internal bool matchWords(string _inputWord, string _dictWord)
        {
            if (_inputWord.Length > _dictWord.Length)
            {
                return false;
            }
            for (int i = _inputWord.Length-1; i > 1; i--)
            {
                if (_inputWord[i] != _dictWord[i])
                {
                    return false;
                }
            }        
            return true;
        } 
    }
}
