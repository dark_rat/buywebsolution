﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TextEditorInintelligence.Classes.DTO
{
    [DataContract]
    internal class DictWord
    {
        [DataMember]
        internal string word;

        [DataMember]
        internal int frequency;

        [DataMember]
        internal int Id;
    }
}
