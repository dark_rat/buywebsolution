﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;

namespace TextEditorInintelligence.Classes.DTO
{
    
    [DataContract]
    internal class DictCollection
    {
        [DataMember]
        internal List<Dict> Dictionaries = new List<Dict>();
    }
}
