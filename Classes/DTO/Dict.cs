﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace TextEditorInintelligence.Classes.DTO
{
    [DataContract]
    internal class Dict
    {
        [DataMember]
        internal List<DictWord> words = new List<DictWord>();

        [DataMember]
        internal string Header;

        [DataMember]
        internal int Id;
    }

}
