﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TextEditorInintelligence.Classes;
using System.IO;

namespace TextEditorInintelligence.Classes.NUnitTests
{
    [TestFixture]
    class InputFileProviderTests
    {
        /// <summary>
        /// exists
        /// </summary>
        #region

        [Test]
        public static void FileNotExists()
        {
            InputFileProvider IFP = new InputFileProvider();
            IFP._pathToFile = "";
            Assert.IsFalse(IFP.exists());
        }

        [Test]
        public static void FileExists()
        {
            InputFileProvider IFP = new InputFileProvider();
            IFP._pathToFile = "input.txt";
            Assert.IsTrue(IFP.exists());
        }

        #endregion

        #region

        public static string createTestFile(int _type)
        {
            string text = "";
            switch (_type)
            {
                case 1:
                    text = "test";
                    break;
            }
            File.AppendAllText("IFPTest.txt", text);
            return text;
        }

        public static void removeTestFile()
        {
            if (File.Exists("IFPTest.txt"))
            {
                File.Delete("IFPTest.txt");
            }
        }
        [Test]
        public static void readLineTest()
        {
            string testString = createTestFile(1);
            InputFileProvider IFP = new InputFileProvider();
            IFP._pathToFile = "IFPTest.txt";
            StringAssert.IsMatch(testString, IFP.readLine(0));
            File.AppendAllText("IFPTest1.txt", IFP.readLine(1));
            Assert.AreEqual(null, IFP.readLine(1));           
            IFP._pathToFile = "IFP.txt";
            Assert.AreEqual(null, IFP.readLine(0));
            removeTestFile();
        }
        #endregion
    }
}
