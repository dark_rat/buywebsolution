﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace TextEditorInintelligence.Classes
{
    /// <summary>
    /// Обеспечивает работу с словарем
    /// </summary>
    class DictionaryProvider
    {
        public DTO.DictCollection collection;

        /// <summary>
        /// Входной файл
        /// </summary>
        public InputFileProvider InputFile { get; set; }

        public DictionaryProvider()
        {
        
        }

        /// <summary>
        /// Количество слов в словаре
        /// </summary>
        private int countWord;

        /// <summary>
        /// Создание коллекции словарей из входного списка слов
        /// </summary>
        /// <param name="IFP">Входной файл</param>
        public void createDictionaryCollection(InputFileProvider IFP)
        {
            collection = new DTO.DictCollection();
            Regex rgx = new Regex(@"^[a-z]{1,15}\s[0-9]", RegexOptions.Singleline);
            string line = "";
            char[] alpha = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            ConcurrentBag<DTO.Dict> concurentDictBag = new ConcurrentBag<DTO.Dict>();
            Parallel.ForEach(alpha, _header =>
            {
                List<string> strCollection = IFP.getDictionaryWords(_header.ToString());                
                DTO.Dict dict = createDictionary(_header.ToString());
                List<DTO.DictWord> words = new List<DTO.DictWord>();
                foreach (string strWord in strCollection)
                {
                    if (!rgx.IsMatch(strWord))
                    {
                        DiagUtils.ErrorLogList.Add(String.Format("Incorrect word in dictionary {0}", line));
                        continue;
                    }
                    DTO.DictWord newWord = createDictWord(strWord);
                    if (newWord.frequency == -1)
                    {
                        DiagUtils.ErrorLogList.Add(String.Format("Incorrect frequency word in dictionary {0}", line));
                        continue;
                    }
                    newWord.Id = this.countWord;
                    this.countWord++;
                    words.Add(newWord);
                }
                dict.words.AddRange(words.OrderByDescending(w => w.frequency).ThenBy(w => w.word));
                concurentDictBag.Add(dict);
            });
            collection.Dictionaries.AddRange(concurentDictBag);
        }

       /// <summary>
       /// Создает словарь
       /// </summary>
       /// <param name="_Header">Заголовок словаря</param>
       /// <returns></returns>
        private DTO.Dict createDictionary(string _Header)
        {
            DTO.Dict newDict = new DTO.Dict();
            newDict.Header = _Header;
            this.countWord = 1;
            return newDict;
        }

        /// <summary>
        /// Создает объект для хранения слова
        /// </summary>
        /// <param name="_Line">Строка (слово+рейтинг)</param>
        /// <returns></returns>
        private DTO.DictWord createDictWord(string _Line)
        {
            int bufferInt;
            DTO.DictWord newWord = new DTO.DictWord();
            newWord.frequency = -1;
            newWord.word = _Line.Split(' ')[0];
            if (int.TryParse(_Line.Split(' ')[1], out bufferInt) && (bufferInt >= 1 && bufferInt <= 1000000))
                newWord.frequency = bufferInt;

            return newWord;
        }

        /// <summary>
        /// Вывод коллекции в файл
        /// </summary>
        /// <param name="_dictCollection"></param>
        /// <param name="_outputFileProvider"></param>
        public void printDictCollection(DTO.DictCollection _dictCollection, OutputFileProvider _outputFileProvider)
        {
            List<string> printList = new List<string>();           
            _dictCollection.Dictionaries.OrderBy(d=>d.Id).ToList().ForEach(printDict =>
            {
                printList.Add(printDict.Header);
                printList.AddRange(printDict.words.Select(w => w.word).ToList());                
            });
            _outputFileProvider.appendFromList(printList);
        }
    }
}
