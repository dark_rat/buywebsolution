﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TextEditorInintelligence.Classes
{
    class DiagUtils
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private NLog.Logger Logger =  NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Секундомер
        /// </summary>
        private Stopwatch swatch;

        /// <summary>
        /// Секундомер 2
        /// </summary>
        private Stopwatch swatchSecond;
        /// <summary>
        /// Функция запустившая таймер
        /// </summary>
        /// 
        private string swatchCustomer;

        /// <summary>
        /// Сообщения логгера Info уровня
        /// </summary>
        public static List<string> InfoLogList = new List<string>();

        /// <summary>
        /// Сообщения логгера Warn уровня
        /// </summary>
        public static List<string> ErrorLogList = new List<string>();

        /// <summary>
        /// Генерация сообщения о оспользовании ОП
        /// </summary>
        public static void LogMemUsage(string _customer)
        {
            System.Diagnostics.Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            long totalBytesOfMemoryUsed = currentProcess.WorkingSet64;
            InfoLogList.Add(String.Format("{1} :Usage mem {0} Mb", (totalBytesOfMemoryUsed / 1024f) / 1024f, _customer));
        }

        /// <summary>
        /// Запись лога в файл
        /// </summary>
        public void writeLogToFile()
        {
            InfoLogList.ForEach(Mes => { Logger.Info(String.Format(Mes)); });
            ErrorLogList.ForEach(Mes =>{ Logger.Warn(String.Format(Mes));});
        }

        /// <summary>
        /// Запуск таймера
        /// </summary>
        /// <param name="_customer"></param>
        public void startSwatch(string _customer)
        {
            this.swatchCustomer = _customer;
            this.swatch = new Stopwatch();
            this.swatch.Start();
        }       

        /// <summary>
        /// Остановка таймера
        /// </summary>
        public void stopSwatch()
        {
            DiagUtils.InfoLogList.Add(String.Format("{1} time: {0}", swatch.Elapsed, this.swatchCustomer));
            this.swatch.Stop();            
        }

        /// <summary>
        /// Запуск таймера 2
        /// </summary>       
        public void startSecondSwatch()
        {           
            this.swatchSecond = new Stopwatch();
            this.swatchSecond.Start();
        }

        /// <summary>
        /// Остановка таймера 2
        /// </summary>
        /// <param name="_message"></param>
        public void stopSecondSwatch(string _message)
        {
            DiagUtils.InfoLogList.Add(String.Format("{1} time: {0}", swatchSecond.Elapsed, _message));
            this.swatchSecond.Stop();           
        }

        public static void Error(Exception _ex)
        {
            DiagUtils.ErrorLogList.Add("Source: " + _ex.Source + " Message:" + _ex.Message);
        }
    }
}
