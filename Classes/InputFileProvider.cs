﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TextEditorInintelligence.Classes
{
    enum TypeWords {Dict, User}
    /// <summary>
    /// Обеспечивает работу с форматом входного файла
    /// </summary>
    class InputFileProvider : FileProvider
    {
        /// <summary>
        /// Корректность входного файла
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// Счетчик прочитанных слов во входном файле словаря
        /// </summary>
        public int readDictCounter { get; set; }

        /// <summary>
        /// Счетчик прочитанных комбинаций во входном файле, введенных пользователем
        /// </summary>
        public int readUserCounter { get; set; }

        /// <summary>
        /// Количество слов в словаре
        /// </summary>
        public int numOfDictionaryWords { get; set; }

        /// <summary>
        /// Количество вводимых пользователем слов 
        /// </summary>
        public int numOfUserWords { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public InputFileProvider()
        {
            _pathToFile = "input.txt";
            IsCorrect = true;           
        }

        /// <summary>
        /// Валидация входного файла
        /// </summary>
        public bool validateFile()
        {
            int bufferInt;
            if (!this.exists())
            {
                DiagUtils.ErrorLogList.Add("Input file not found");
                return false;
            }
            if (int.TryParse(this.readLine(0), out bufferInt) && (bufferInt >= 1 && bufferInt <= 100000))
                numOfDictionaryWords = bufferInt;
            else
            {
                DiagUtils.ErrorLogList.Add("Incorrect number of dictionary words in input file");
                IsCorrect = false;
            }
            if (int.TryParse(this.readLine(numOfDictionaryWords + 1), out bufferInt) && (bufferInt >= 1 && bufferInt <= 15000))
                numOfUserWords = bufferInt;
            else
            {
                DiagUtils.ErrorLogList.Add("Incorrect number of user words in input file");
                IsCorrect = false;
            }
            return this.IsCorrect;
        }

        /// <summary>
        /// Получить список введеных пользователем слов
        /// </summary>
        /// <returns></returns>
        public List<DTO.DictWord> getUserDictWordsList()
        {
            List<DTO.DictWord> resultDict = new List<DTO.DictWord>();
            int count = 0;
            Regex rgx = new Regex(@"^[^a-z]{1,15}", RegexOptions.Singleline);
            foreach (string strWord in this.getWordsCollection(" ", TypeWords.User))
            {
                if (!rgx.IsMatch(strWord))
                {
                    count++;
                    DTO.DictWord word = new DTO.DictWord();
                    word.Id = count;
                    word.word = strWord;
                    resultDict.Add(word);
                }
            }
            return resultDict;

        }

        /// <summary>
        /// Получить список слов словаря
        /// </summary>
        /// <param name="_header">Начало слов</param>
        /// <returns></returns>
        public List<string> getDictionaryWords(string _header)
        {
            return this.getWordsCollection(_header, TypeWords.Dict);
        }

        /// <summary>
        /// Получить список слов
        /// </summary>
        /// <param name="_header">Начало слов</param>
        /// <param name="type">тип слов (Словарь\Пользователь)</param>
        /// <returns></returns>
        private List<string> getWordsCollection(string _header, TypeWords type)
        {
            switch (type)
            { 
                case TypeWords.Dict:
                    return readLinesStartsWith(_header, " ", true);
                case TypeWords.User:
                    return _header != " " ? readLinesStartsWith(_header, " ", false) : readLinesStartsWith(" ", false);
                default:
                    return new List<string>();
            }
        }     

    }
}
