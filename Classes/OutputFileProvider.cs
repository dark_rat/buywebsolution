﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextEditorInintelligence.Classes
{
    /// <summary>
    /// Обеспечивает работу с выходным файлом
    /// </summary>
    public class OutputFileProvider : FileProvider
    {
        public OutputFileProvider()
        {
            _pathToFile = "Output.txt";
        }
    }
}
