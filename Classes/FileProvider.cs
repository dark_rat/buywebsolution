﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TextEditorInintelligence.Classes
{
    /// <summary>
    /// Обеспечивает функции чтения - записи в текстовые файлы
    /// </summary>
    public class FileProvider
    {        
        /// <summary>
        /// Путь к файлу
        /// </summary>
        public string _pathToFile;

        /// <summary>
        /// Обеспечивает функции чтения - записи в текстовые файлы
        /// </summary>
        public FileProvider()
        {

        }
        
        /// <summary>
        /// Обеспечивает функции чтения - записи в текстовые файлы
        /// </summary>
        /// <param name="pathToFile">Полный путь к файлу</param>
        public FileProvider(string pathToFile)
        {
            _pathToFile = pathToFile;
        }

        /// <summary>
        /// Возвращает строку, начинающуюся со строки 
        /// </summary>
        /// <param name="_inputString">Строка поиска</param>
        /// <returns></returns>
        public string readLineStartsWith(string _inputString)
        {
            return this.exists() ? File.ReadLines(_pathToFile).FirstOrDefault(s => s.StartsWith(_inputString)) : null;
        }

        /// <summary>
        /// Возвращает строки, начинающиеся со строки 
        /// </summary>
        /// <param name="_headerString">Строка поиска</param>
        /// <returns></returns>
        public List<string> readLinesStartsWith(string _headerString, string _containString, bool isContain)
        {
            return this.exists() ?
                (isContain ? File.ReadLines(_pathToFile).Where(s => s.StartsWith(_headerString) && s.Contains(_containString)).ToList() :
                             File.ReadLines(_pathToFile).Where(s => s.StartsWith(_headerString) && !s.Contains(_containString)).ToList())
                                 : new List<string>();
        }

        public List<string> readLinesStartsWith(string _containString, bool isContain)
        {
            return this.exists() ?
                (isContain ? File.ReadLines(_pathToFile).Where(s =>  s.Contains(_containString)).ToList() :
                             File.ReadLines(_pathToFile).Where(s =>  !s.Contains(_containString)).ToList())
                                 : new List<string>();
        }
        /// <summary>
        /// Возвращает строку с номером
        /// </summary>
        /// <param name="lineNum">Номер строки</param>
        /// <returns></returns>
        public string readLine(int lineNum)
        {
            return this.exists() ? File.ReadLines(_pathToFile).Skip(lineNum).FirstOrDefault() : null;
        }       

        /// <summary>
        /// Запись листа строк в текстовый файл
        /// </summary>
        /// <param name="_inputList">Входной набор строк</param>
        public void appendFromList(List<string> _inputList)
        {
            try
            {
                File.AppendAllLines(_pathToFile, _inputList);
            }
            catch (Exception ex)
            {
                DiagUtils.Error(ex);
            }
        }

        /// <summary>
        /// Запись листа строк в текстовый файл
        /// </summary>
        /// <param name="_inputString">Входная строка</param>
        public void appendFromList(string _inputString)
        {            
            this.appendFromList(new List<string> { _inputString });           
        }

        /// <summary>
        /// Обнуление файла
        /// </summary>
        public void reset()
        {
            if (File.Exists(this._pathToFile))
            {
                try
                {
                    File.Delete(_pathToFile);
                }
                catch (Exception ex)
                {
                    DiagUtils.Error(ex);
                }
            }
        }

        /// <summary>
        /// Проверка существования файла
        /// </summary>
        /// <returns></returns>
        public bool exists()
        {
            return File.Exists(this._pathToFile);
        }
    }
}
